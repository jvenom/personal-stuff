from functools import reduce
from pathlib import Path


REPLACE_PATTERNS = (
    ('|', '_'),
    (' ', '-'),
    ("'", ""),
    (':', ''),
    ('(', ''),
    (')', ''),
    ('&', 'and'),
)


def main():
    music_root = Path('~/Music/kgsr_broadcast_20')
    for child in music_root.expanduser().iterdir():
        print(f'Processing directory: {child}')
        for track in child.iterdir():
            current_name = track.name
            new_name = reduce(
                lambda string, patterns: string.replace(*patterns),
                REPLACE_PATTERNS,
                current_name
            )
            new_path = music_root / child / new_name
            track.rename(new_path)
            print(f'Renamed {track} to {new_path}')


if __name__ == '__main__':
    main()
   