# rename-music

I wrote a blog post about this! Check it out [here](https://jvenom.gitlab.io/venomous-software/blog/python-reduce/).

## The problem

This [script](rename_tracks.py) renames a bunch of `*.ogg` tracks in two directories. For some reason I burned a CD and named all tracks with the pattern `track number|artist|title`, where `artist` and `title` could have spaces, parentheses, colons, apostrophes, and other nasty special characters. This was causing me all kinds of problems when I was trying to copy these files around on my Linux machine. I wanted to rename all these files to something more sensible, while still retaining the same track_number/artist/tile structure in the name.

## Current names, output from ls (:nauseated_face:)
<details>
<summary>Click to expand</summary>

```
'10|Fritz & The Tantrums|Don'\''t Gotta Work It Out.ogg'
'11|Foster The People|Don'\''t Stop (Color On The Walls).ogg'
'12|Walk The Moon|Anna Sun.ogg'
'13|Fun|We Are Young.ogg'
'14|Ingrid Michaelson|Blood Brothers.ogg'
'15|Shawn Colvin|All Fall Down.ogg'
'16|The Jayhawks|She Walks In So Many Ways.ogg'
'17|Ed Sheeran|The A Team.ogg'
'18|Selah Sue|Raggamuffin.ogg'
'19|Kat Edmonson|I Don'\''t Know.ogg'
'1|Gary Clark Jr|Bright Lights.ogg'
'2|Gomez|Options.ogg'
'3|Charlie Mars|How I Roll.ogg'
'4|The Shins|Simple Song.ogg'
'5|Ben Kweller|Jealous Girl.ogg'
'6|Keane|Silenced By The Night.ogg'
'7|Ben Howard|Black Flies.ogg'
'8|The Head And The Heart|Down In The Valley.ogg'
'9|Michael Kiwanuka|I'\''m Getting Ready.ogg'
'10|Sheryl Crow|Summer Day.ogg'
'11|Damien Rice|Cold Water.ogg'
'12|Ray LaMontagne|Jolene.ogg'
'13|Kris Kristofferson|The Pilgrim: Chapter 33.ogg'
'14|Norah Jones|Lonestar.ogg'
'15|Lyle Lovett|My Baby Don'\''t Tolerate.ogg'
'16|Lucinda Williams|Changed The Locks.ogg'
'17|Big Head Todd & The Monsters|Broken Hearted Savior.ogg'
'18|Ryan Bingham|Depression.ogg'
'19|Steve Earle|Tennessee Blues.ogg'
'1|Ryan Adams|Everybody Knows.ogg'
'20|Ryan Adams|KGSR Rainy Day Talkin'\'' Blues.ogg'
'2|Jack Johnson|Flake.ogg'
'3|Aimee Mann|Little Bombs.ogg'
'4|David Gray|Ain'\''t No Love.ogg'
'5|Sarah McLachlan|Adia.ogg'
'6|Bob Schneider|Changing Your Mind.ogg'
'7|Sara Bareilles|Many The Miles.ogg'
'8|Death Cab For Cutie|Crooked Teeth.ogg'
'9|Spoon|Don'\''t You Evah.ogg'
```
</details>

## reduce()

The best nugget from this script is the usage of `functools.reduce()` to replace multiple patterns in a string, without needless duplication. These are all the patterns I wanted to replace in the file names:
```python
REPLACE_PATTERNS = (
    ('|', '_'),
    (' ', '-'),
    ("'", ""),
    (':', ''),
    ('(', ''),
    (')', ''),
    ('&', 'and'),
)
```

My first attempt was a naive approach, like this:
```python
for track in folder:
    new_name = track.replace('|', '_')
    new_name = new_name.replace(' ', '-')
    new_name = new_name.replace("'", "")
    # And so on
```

I thought it'd be cleaner if I defined all the patterns in a separate structure, and then updated them all at once. I chose a tuple of 2-tuples, where the first element in each 2-tuple is the special character, and the second element is what to replace the special character with. I tried to write a recursive function that could iteratively process all these values in turn, but that didn't quite work out. I thought to myself, "This seems like a `reduce()` problem." Eventually I found [this Stack Overflow answer](https://stackoverflow.com/a/9479972) that captured the syntax of what I was trying to do.

## pathlib

This was also an opportunity for me to play around more with Python 3's `pathlib` module. I find working with `Path`s much cleaner than Python 2's `os.path`, `os.*` and `shutil` shenanigans.

## Output

Here's what the script looks like in action. The left path is the old file name (it makes my eyes hurt just looking at it!), and the right is the new name. Now I'm free to copy these songs wherever I want without Linux complaining.
<details>
<summary>Click to expand</summary>

```
(music-rename-oFV8vvVg-py3.9) [josh@localhost-live music_rename]$ /home/josh/.cache/pypoetry/virtualenvs/music-rename-oFV8vvVg-py3.9/bin/python /home/josh/codebase/python/music_rename/rename_tracks.py
Processing directory: /home/josh/Music/kgsr_broadcast_20/disc2
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/9|Spoon|Don't You Evah.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/9_Spoon_Dont-You-Evah.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/6|Bob Schneider|Changing Your Mind.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/6_Bob-Schneider_Changing-Your-Mind.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/3|Aimee Mann|Little Bombs.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/3_Aimee-Mann_Little-Bombs.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/13|Kris Kristofferson|The Pilgrim: Chapter 33.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/13_Kris-Kristofferson_The-Pilgrim-Chapter-33.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/8|Death Cab For Cutie|Crooked Teeth.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/8_Death-Cab-For-Cutie_Crooked-Teeth.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/5|Sarah McLachlan|Adia.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/5_Sarah-McLachlan_Adia.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/11|Damien Rice|Cold Water.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/11_Damien-Rice_Cold-Water.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/18|Ryan Bingham|Depression.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/18_Ryan-Bingham_Depression.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/7|Sara Bareilles|Many The Miles.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/7_Sara-Bareilles_Many-The-Miles.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/17|Big Head Todd & The Monsters|Broken Hearted Savior.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/17_Big-Head-Todd-and-The-Monsters_Broken-Hearted-Savior.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/1|Ryan Adams|Everybody Knows.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/1_Ryan-Adams_Everybody-Knows.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/19|Steve Earle|Tennessee Blues.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/19_Steve-Earle_Tennessee-Blues.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/10|Sheryl Crow|Summer Day.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/10_Sheryl-Crow_Summer-Day.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/14|Norah Jones|Lonestar.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/14_Norah-Jones_Lonestar.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/12|Ray LaMontagne|Jolene.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/12_Ray-LaMontagne_Jolene.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/15|Lyle Lovett|My Baby Don't Tolerate.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/15_Lyle-Lovett_My-Baby-Dont-Tolerate.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/4|David Gray|Ain't No Love.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/4_David-Gray_Aint-No-Love.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/2|Jack Johnson|Flake.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/2_Jack-Johnson_Flake.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/16|Lucinda Williams|Changed The Locks.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/16_Lucinda-Williams_Changed-The-Locks.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc2/20|Ryan Adams|KGSR Rainy Day Talkin' Blues.ogg to /home/josh/Music/kgsr_broadcast_20/disc2/20_Ryan-Adams_KGSR-Rainy-Day-Talkin-Blues.ogg
Processing directory: /home/josh/Music/kgsr_broadcast_20/disc1
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/7|Ben Howard|Black Flies.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/7_Ben-Howard_Black-Flies.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/8|The Head And The Heart|Down In The Valley.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/8_The-Head-And-The-Heart_Down-In-The-Valley.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/19|Kat Edmonson|I Don't Know.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/19_Kat-Edmonson_I-Dont-Know.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/16|The Jayhawks|She Walks In So Many Ways.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/16_The-Jayhawks_She-Walks-In-So-Many-Ways.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/6|Keane|Silenced By The Night.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/6_Keane_Silenced-By-The-Night.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/13|Fun|We Are Young.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/13_Fun_We-Are-Young.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/3|Charlie Mars|How I Roll.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/3_Charlie-Mars_How-I-Roll.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/10|Fritz & The Tantrums|Don't Gotta Work It Out.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/10_Fritz-and-The-Tantrums_Dont-Gotta-Work-It-Out.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/17|Ed Sheeran|The A Team.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/17_Ed-Sheeran_The-A-Team.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/2|Gomez|Options.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/2_Gomez_Options.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/9|Michael Kiwanuka|I'm Getting Ready.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/9_Michael-Kiwanuka_Im-Getting-Ready.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/1|Gary Clark Jr|Bright Lights.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/1_Gary-Clark-Jr_Bright-Lights.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/5|Ben Kweller|Jealous Girl.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/5_Ben-Kweller_Jealous-Girl.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/4|The Shins|Simple Song.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/4_The-Shins_Simple-Song.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/11|Foster The People|Don't Stop (Color On The Walls).ogg to /home/josh/Music/kgsr_broadcast_20/disc1/11_Foster-The-People_Dont-Stop-Color-On-The-Walls.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/12|Walk The Moon|Anna Sun.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/12_Walk-The-Moon_Anna-Sun.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/18|Selah Sue|Raggamuffin.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/18_Selah-Sue_Raggamuffin.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/15|Shawn Colvin|All Fall Down.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/15_Shawn-Colvin_All-Fall-Down.ogg
Renamed /home/josh/Music/kgsr_broadcast_20/disc1/14|Ingrid Michaelson|Blood Brothers.ogg to /home/josh/Music/kgsr_broadcast_20/disc1/14_Ingrid-Michaelson_Blood-Brothers.ogg
```
<details>