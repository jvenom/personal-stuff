# personal-stuff

These are my personal projects and utility scripts that don't neatly fit in any of my other GitLab projects or [GitHub repos](https://github.com/joshvernon?tab=repositories). Mainly I'm trying to make it easier on myself when I switch machines or want to look up examples of old code to inform new projects. You're welcome to poke around and use whatever you like. I make no guarantees that anything will work or be kept up-to-date.

I'll try to include additional `README`s in the sub-projects where it makes sense.

